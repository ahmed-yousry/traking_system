<!DOCTYPE html>

<html>

  <head>
    <meta charset="UTF-8" />
    <!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>ierek traking system</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">




  </head>

<body>



  <div class="container">

      <h1 class="text-center py-2">ierek traking system  </h1>



      <div class="card">

        <div class="card-header">

          <form method="POST" action="<?php echo e(route('generate.shorten.link.post')); ?>">

              <?php echo csrf_field(); ?>

              <div class="input-group mb-3">

                <input type="text" name="name" class="form-control" placeholder="Enter Name URL" aria-label="Recipient's username" aria-describedby="basic-addon2">


                <input type="text" name="link" class="form-control" placeholder="Enter URL" aria-label="Recipient's username" aria-describedby="basic-addon2">

                <div class="input-group-append">

                  <button class="btn btn-success" type="submit">Generate Shorten Link</button>

                </div>
              </div>

          </form>

        </div>

        <div class="card-body">



              <?php if(Session::has('success')): ?>

                  <div class="alert alert-success py-1">

                      <p class="m-0"><?php echo e(Session::get('success')); ?></p>

                  </div>

              <?php endif; ?>



              <table  class="table table-bordered table-hover table-sm">

                  <thead>

                      <tr>

                          <th>ID</th>
                          <th>Name of link</th>
                          <th>Short Link</th>

                          <th>Link</th>
                          <th>Report</th>

                      </tr>

                  </thead>

                  <tbody>

                      <?php $__currentLoopData = $shortLinks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                          <tr>

                              <td><?php echo e(@$row->id); ?></td>
  <td><?php echo e(@$row->name); ?></td>
                              <td><a href="<?php echo e(route('shorten.link', @$row->code)); ?>" target="_blank"><?php echo e(route('shorten.link', @$row->code)); ?></a></td>

                              <td><?php echo e(@$row->link); ?></td>

                              <td><a href="<?php echo e(route('report.link',@$row->id)); ?>" target="_blank">Show Report</a></td>

                          </tr>

                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                  </tbody>

              </table>

        </div>

      </div>



  </div>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>

</html>
