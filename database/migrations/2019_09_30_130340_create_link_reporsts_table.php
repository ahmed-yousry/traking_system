<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinkReporstsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_reporsts', function (Blueprint $table) {

            $table->increments('id');
            $table->string('short_links_id');
            $table->string('view')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('date')->nullable();


            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('ip')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('link_reporsts');
    }
}
