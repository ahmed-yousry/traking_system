<!DOCTYPE html>

<html>

<head>
  <meta charset="UTF-8" />
  <!-- Required meta tags -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>ierek traking system</title>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">





</head>

<body>



<div class="container">

    <h1 class="text-center py-2">ierek traking system  </h1>



    <div class="card">

      <div class="card-header">
        <div class="d-flex">
          <div class="bg-white px-2 py-1  border-secondary border rounded">
            Count : {{ @$count }}
          </div>
        </div>


      </div>

      <div class="card-body">



            @if (Session::has('success'))

                <div class="alert alert-success">

                    <p>{{ Session::get('success') }}</p>

                </div>

            @endif

            <div class="table-responsive">
            <button onclick="exportTableToExcel('tblData')">Export Table Data To Excel File</button>
              <table class="table table-bordered table-sm table-hover "  id="tblData" >

                  <thead>

                      <tr>

                          <th>ID</th>

                          <th>Ip</th>
                          <th>country</th>
                          <th>city</th>
                          <th>Date</th>
                          <th>latitude</th>
                          <th>longitude</th>













                      </tr>

                  </thead>

                  <tbody>

                      @foreach($report as $row)

                          <tr>

                              <td>{{ @$row->id }}</td>
                              <td>{{ @$row->ip }}</td>
                              <td>{{ @$row->country }}</td>
                              <td>{{ @$row->city }}</td>
                              <td>{{ @$row->date }}</td>

                              <td>{{ @$row->latitude }}</td>
                              <td>{{ @$row->longitude }}</td>









                          </tr>


                      @endforeach

                  </tbody>

              </table>
            </div>


      </div>

    </div>



</div>

<script>
function exportTableToExcel(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

    // Specify file name
    filename = filename?filename+'.xls':'excel_data.xls';

    // Create download link element
    downloadLink = document.createElement("a");

    document.body.appendChild(downloadLink);

    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

        // Setting the file name
        downloadLink.download = filename;

        //triggering the function
        downloadLink.click();
    }
}
</script>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>

</html>
