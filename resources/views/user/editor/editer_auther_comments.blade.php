<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

        <title>Submit</title>

        <!-- For icons -->
      @include('user.includes.css')
    </head>
    <body>

     @include('user.includes.header')

        <div class="w-100 container reviews-a" bgpage="reviews-a" submit-a="">
            <section class="py-3 row sc-global">
                <form class="col-12 tr-forms re-edit" method="post" action="{{url('home/editor/Review/editer_auther_comments')}}" >
                	 {{csrf_field()}}
                    <div class="row  w-100 sb-content m-0">
                        <div class="col-12 sb-header">
                            <i class="fas fa-user-friends"></i>  Editor/Author Correspondence
                        </div>
                        <!-- comment -->


                        <div class="col-12">
                        	@if(!empty($editer_auther_comments->first()->id))

                            	  @foreach($editer_auther_comments as $editer_auther_comment)
                            <div class="alert alert-secondary alert-com" role="alert">
                            	

{{$editer_auther_comment->comments}}
                                <span class="date-alert">By the {{$editer_auther_comment->get_user->name}}  {{$editer_auther_comment->created_at}}</span>


                               



                              
                            </div>

                                
 @endforeach



                            	 @endif

                        </div>

                        <div class="col-12 mt-3">
                            <textarea class="form-control" name="comments" rows="3" style="min-height: 126px !important" placeholder="add your Comments ..." required></textarea>
                        </div>
                       

                        <input type="hidden" name="paper_id" value="{{$paper_id}}">
                        <input type="hidden" name="user_id" value="{{$user_id}}">
                         <input type="hidden" name="seg_id" value="{{$seg_id}}">


                        


                        <div class="col-12 text-center mt-2">
                            <button type="submit" class="btn btn-outline-info m-2"> <i class="fas fa-comment"></i> 
                                Send      </button>
                        </div>
                    </div>
                </form>

          
            </section>

        </div>

        <!-- Script -->
 @include('user.includes.js')
    </body>
</html>