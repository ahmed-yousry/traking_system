<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

        <title>Submit</title>

        <!-- For icons -->
    @include('user.includes.css')
    </head>
    <body>

       @include('user.includes.header')
        <div class="w-100 container reviews-a" bgpage="reviews-a" submit-a="">
            <section class="py-3 row sc-global">


                <div class="col-12 tr-forms re-edit e-coding">
                    <div class="alert alert-secondary alert-com mb-2" role="alert">




                            Title of the paper : {{$paper->get_metadata->Title}}
                            <br>

                            Abstract : {{$paper->get_metadata->Abstract}}


 <!-- @if(isset($paper->get_auther_reply->filename))


        <a class="d-block go-link" href="{{Request::root()}}/submit_auther_review/{{$paper->get_auther_reply->filename}}" target="_blank" download="{{$paper->get_auther_reply->filename}}"> <i class="fas fa-download"></i> Download</a>

   @endif -->

                    </div>
                    <!-- <div class="w-100 text-center mb-3">
                    <a href="" class="btn btn-outline-info m-2"> <i class="fas fa-edit"></i>
                            Update my review
                        </a>
                    </div> -->
                    <div class="row w-100 sb-content m-0">
                        <div class="col-12 sb-header">
                            <i class="fas fa-layer-group"></i> My review
                        </div>
                        <div class="col-12 ts-input text-secondary">











                            <div class="w-100  py-3">
                                <div class="w-100 row data-row">
                                    <div class="data-col col-6 col-md-4 col-lg-3">Reviewer's expertise in this research study</div>
                                    <div class="data-col col-6 col-md-8 col-lg-9">{{$myreviews->Reviewer_expertise}}</div>
                                </div>
                                <div class="w-100 row data-row">
                                    <div class="data-col col-6 col-md-4 col-lg-3">Overall / AVG Mark</div>
                                    <div class="data-col col-6 col-md-8 col-lg-9">	{{$myreviews->Overall_evaluation}}</div>
                                </div>

                                <div class="w-100 row data-row">
                                    <div class="data-col col-6 col-md-4 col-lg-3">Originality</div>
                                    <div class="data-col col-6 col-md-8 col-lg-9">{{$myreviews->Originality}}</div>
                                </div>

                                <div class="w-100 row data-row">
                                    <div class="data-col col-6 col-md-4 col-lg-3">Quality</div>
                                    <div class="data-col col-6 col-md-8 col-lg-9">{{$myreviews->Quality}}</div>
                                </div>


                                <div class="w-100 row data-row">
                                    <div class="data-col col-6 col-md-4 col-lg-3">English Quality</div>
                                    <div class="data-col col-6 col-md-8 col-lg-9">{{$myreviews->English_Quality}}</div>
                                </div>


                                <div class="w-100 row data-row">
                                    <div class="data-col col-6 col-md-4 col-lg-3">Overall evaluation</div>
                                    <div class="data-col col-6 col-md-8 col-lg-9">{{$myreviews->Overall_evaluation}}</div>
                                </div>

                                <div class="w-100 row data-row">
                                    <div class="data-col col-6 col-md-4 col-lg-3">Detailed comments (shown to the authors)</div>
                                    <div class="data-col col-6 col-md-8 col-lg-9">
                                           {{$myreviews->Detailed_comments}}
                                    </div>
                                </div>


                                <div class="w-100 row data-row">
                                    <div class="data-col col-6 col-md-4 col-lg-3">Comments to the Proceedings Editor (not shown to the authors)</div>
                                    <div class="data-col col-6 col-md-8 col-lg-9">
                                          {{$myreviews->Comments_to_Proceedings}}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


            </section>

        </div>

        <!-- Script -->
 @include('user.includes.js')
    </body>
</html>
