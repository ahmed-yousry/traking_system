<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

        <title>Editor</title>

        <!-- For icons -->
    @include('user.includes.css')

        <script src="./js/tp.js"></script>
    </head>
    <body>
       @include('user.includes.header')

        <div class="w-100 container reviews-a" bgpage="reviews-a">
            <section class="py-3">
                <!-- ______Head Track________ -->
                <div class="col-12 d-flex  pb-5 pt-3 justify-content-center align-items-center align-items-stretch">
                    <ul class="track-st sk-lan">
                        <li><a class="ac-li " href="{{url('/home/editor')}}">Unassigned</a></li>
                        <li ><a href="{{url('/home/editor/in-review')}}">In Review</a></li>
                          <li ><a  href="{{url('/home/editor/Archive')}}">Archive</a></li>
                    </ul>
                </div>

                  <div class="w-100 dataTable py-3 table-responsive" main="tables">
                        <table id="DataTable" Dtable="datatable" class="display table  table-bordered   table-hover" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Paper Number</th>
                                        <th>Title of the paper</th>
                                        <th>List of authors</th>
                                        <th>Submission track</th>
                                        <th>Submission date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                 @foreach($papers as $paper)
                                    <tr>


                                        <td>{{$paper->id}}</td>
                                        <td>{{@$paper->get_metadata->Title}}</td>
                                        <td>{{@$paper->get_user->name}}</td>
                                        <td> {{@$paper->get_Track_Selction->track_name}} </td>
                                        <td>{{@$paper->created_at}}</td>
                                        <td>
                                            <a class="d-block go-link" href="{{url('home/editor/View_Submission')}}/{{$paper->id}} "> <i class="fas fa-edit"></i> View Submission</a>
                                        </td>

                                    </tr>
                                 @endforeach
                                </tbody>
                            </table>
                  </div>

            </section>
        </div>
        <!-- <div class="scroll-table">
            <div class="sc-table"><i class="fas fa-chevron-circle-left"></i></div>
            <div class="sc-table tx-scroll"><i class="fas fa-cog fa-spin"></i></div>
            <div class="sc-table"><i class="fas fa-chevron-circle-right"></i></div>
        </div> -->
        <!-- Script -->
 @include('user.includes.js')
    </body>
</html>
