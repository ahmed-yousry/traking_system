<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

        <title>Performer Ierek</title>

  @include('user.includes.css')
    </head>
    <body>
        <section class="bgApp" bgpage="bg-app">
            <div class="con-app row m-0 container-fluid ">
                <form class="row form-app col-12 p-0 col-md-7 col-lg-5 mx-auto was-validated" method="POST" action="{{ route('password.request') }}" >
                  @csrf

                  <input type="hidden" name="token" value="{{ $token }}">

                    <h1 class="col-12 text-center  mb-5">
                        <div class="brand-e">
                                <div class="ierek">
                                            IEREK
                                </div>
                                <div class="ierek-s">
                                    S
                                </div>
                        </div>
                    </h1>

                    <div class="col-12">



                            <div class="w-1000">
                                <div class="form-group">
                                    <span class="ts-icon"><i class="fas fa-at"></i></span>





  <input id="email" type="email" placeholder="Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>



                                </div>
                                <!-- Validation -->
                                <div class="invalid-feedback">
                                    Please enter a .....
                                </div>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif


                            </div>

                            <div class="w-100">
                                <div class="form-group">
                                    <span class="ts-icon"><i class="fas fa-unlock-alt"></i></span>



                                    <input id="password" placeholder="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif


                                </div>
                                <!-- Validation -->
                                <div class="invalid-feedback">
                                    Please enter a .....
                                </div>
                            </div>

                            <div class="w-100">
                                <div class="form-group">
                                    <span class="ts-icon"><i class="fas fa-unlock-alt"></i></span>
                                <input id="password-confirm" placeholder="password confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                                <!-- Validation -->
                                <div class="invalid-feedback">
                                    Please enter a ....
                                </div>
                            </div>

                            <!-- <button type="submit" class="btn btn-primary">
                                {{ __('Reset Password') }}
                            </button> -->

                            <button type="submit" class="btn w-100"><i class="fas fa-redo-alt"></i>
                               {{ __('Reset Password') }}

                            </button>
                    </div>
                </form>
            </div>
        </section>
        <!-- Script -->
 @include('user.includes.js')
    </body>
</html>
