@extends('admin.layouts.master')
@section('title')
    المواد الدراسية
@endsection
@section('page-header')
    <section class="content-header">
        <h1>
            المواد الدراسية
     <small></small></h1>

    </section>
@endsection

@section('content')


    <section class="content">

        <div class="row">
        <div class="col-md-12">
            @if(\Illuminate\Support\Facades\Auth::guard('admin')->user()->can('subject.create'))
                <a href="{{url('/admin/subject/create')}}" class="btn btn-primary pull-right margin-bottom">
                    <i class="fa fa-plus"></i>
                    اضافة مادة جديدة
                </a>
            @endif

        </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    {{--<div class="box-header">--}}
                        {{--<h3 class="box-title">--}}
                        {{--</h3>--}}
                        {{--<div class="box-tools">--}}
                            {{--<div class="input-group input-group-sm" style="width: 150px;">--}}
                                {{--<input type="text" name="table_search" class="form-control pull-right"--}}
                                       {{--placeholder="Search">--}}

                                {{--<div class="input-group-btn">--}}
                                    {{--<button type="submit" class="btn btn-default">--}}
                                        {{--<i class="fa fa-search"></i></button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th>اسم المادة</th>
                                <th>عمليات</th>
                            </tr>
                            @foreach($subjects as $sub)
                                <tr>
                                    <td>{{$sub->name}}</td>
                                    <td>
                                        @if(\Illuminate\Support\Facades\Auth::guard('admin')->user()->can('subject.update'))
                                            <a href="{{url('/admin/subject/'.$sub->id.'/edit')}}" class="btn btn-info btn-circle"><i class="fa fa-edit"></i></a>
                                        @endif
                                        @if(\Illuminate\Support\Facades\Auth::guard('admin')->user()->can('subject.delete'))
                                                <a href="{{url('admin/subject/'.$sub->id.'/delete')}}" class="btn btn-danger btn-circle"><i class="fa fa-trash-o"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        {{ $subjects->links() }}
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>

        <br>

    </section>

@endsection

@section('css')
@endsection

@section('js')
@endsection
