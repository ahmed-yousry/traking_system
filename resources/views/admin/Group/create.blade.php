@extends('admin.layouts.master')
@section('title')
    اضافة مجموعة جديدة
@endsection
@section('page-header')
    <section class="content-header">
        <h1>
          اضافة مجموعة جديدة
            <small></small>
        </h1>

    </section>
@endsection

@section('content')

    <section class="content" id="app">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="post" action="{{url('/admin/group')}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="edu_id" class="col-sm-1 control-label">المرحلة</label>
                                <div class="col-sm-5">
                                    <select name="edu_id"  v-on:change="getClass" v-model="edu_id" class="form-control">
                                        <option v-for="i in educations" v-bind:value="i.id" >@{{i.name}}</option>
                                    </select>
                                </div>

                                <label for="class_id" class="col-sm-1 control-label">السنة الدراسية</label>
                                <div class="col-sm-5">
                                    <select name="class_id" id="edu_id" v-on:change="getSubject" v-model="class_id" class="form-control ">
                                        <option v-for="x in classes" v-bind:value="x.id">@{{x.name}}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="category" class="col-sm-1 control-label">المادة</label>
                                <div class="col-sm-5">
                                    <select name="subject_id" id="subject_id" v-on:change="getTeacher" v-model="subject_id" class="form-control ">
                                            <option v-for="s in subjects" v-bind:value="s.id">@{{s.name}}</option>
                                    </select>
                                </div>

                                <label for="edu_id" class="col-sm-1 control-label">المدرس</label>
                                <div class="col-sm-5">
                                    <select name="teacher_id" id="edu_id" v-model="teacher_id" class="form-control">
                                        <option v-for="s in teachers" v-bind:value="s.id">@{{s.fname}}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">

                                <label for="title" class="col-sm-1 control-label">موعد البدأ</label>
                                <div class="col-sm-5">
                                    <input type="number" name="from" v-model="from" class="form-control" id="title" placeholder="موعد بدأ المجموعة" value="{{ old('from') }}" required autofocus>
                                    {{--@if ($errors->has('from'))--}}
                                        {{--<span class="help-block  ">--}}
                                            {{--<strong>اسم المجموعة مطلوب</strong>--}}
                                        {{--</span>--}}
                                    {{--@endif--}}
                                </div>
                                <label for="title" class="col-sm-1 control-label">موعد الانتهاء</label>
                                <div class="col-sm-5 {{ $errors->has('to') ? ' has-error' : '' }}">
                                    <input type="number" name="to"  v-model="to"class="form-control" id="title" placeholder="موعد انتهاء المجموعة" value="{{ old('to') }}" required autofocus>
                                    @if ($errors->has('to'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('to') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                              <div class="form-group">
                                <label for="category" class="col-sm-1 control-label">اسم  المجموعه</label>
                                <div class="col-sm-11 ">
                                    <input type="text" name="name" v-model="name" id="name" class="form-control ">
                                </div>
                            </div>



                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" :disabled="!isComplete" v-on:click.prevent="addGroup"  class="btn btn-info center-block">اضافة المجموعة <i class="fa fa-save" style="margin-left: 5px"></i></button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->
                <!-- general form elements disabled -->

                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>

@endsection

@section('css')
@endsection

@section('js')

    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script src="https://unpkg.com/vue-swal"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-select/2.5.1/vue-select.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.15.2/axios.js"></script>
    <script type="text/javascript" src="{{asset('assets/main/group.js')}}"></script>




@endsection
