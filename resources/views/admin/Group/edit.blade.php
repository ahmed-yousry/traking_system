@extends('admin.layouts.master')
@section('title')
    تعديل مجموعة
@endsection
@section('page-header')
    <section class="content-header">
        <h1>
            تعديل مجموعة جديدة
            <small></small>
        </h1>

    </section>
@endsection

@section('content')

    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="post" action="{{url('/admin/group/'.$group->id)}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input name="_method" type="hidden" value="PATCH">


                        <div class="box-body">
                            <div class="form-group">
                                <label for="title" class="col-sm-1 control-label"> اسم المجموعه</label>
                                <div class="col-sm-5 {{ $errors->has('from') ? ' has-error' : '' }}">
                                    <input type="text" name="name"  class="form-control" id="title" placeholder="اسم المجموعه " value="{{$group->name}}" required autofocus>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                        <label for="category" class="col-sm-1 control-label">السنة</label>
                                        <div class="col-sm-5 ">
                                            <select name="class_room_id" id="class_room_id" class="form-control ">
                                                @foreach($classes as $class)
                                                    <option value="{{$class->id}}" @if($class->id == $group->class_room_id) selected @endif>{{$class->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                            </div>


                            <div class="form-group">
                                <label for="title" class="col-sm-1 control-label">موعد البدأ</label>
                                <div class="col-sm-5 {{ $errors->has('from') ? ' has-error' : '' }}">
                                    <input type="text" name="from"  class="form-control" id="title" placeholder="موعد بدأ المجموعة" value="{{$group->from}}" required autofocus>
                                    @if ($errors->has('from'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('from') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <label for="title" class="col-sm-1 control-label">موعد الانتهاء</label>
                                <div class="col-sm-5 {{ $errors->has('to') ? ' has-error' : '' }}">
                                    <input type="text" name="to" class="form-control"  id="title" placeholder="موعد انتهاء المجموعة" value="{{$group->to}}" required autofocus>
                                    @if ($errors->has('to'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('to') }}</strong>
                                        </span>
                                    @endif
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="category" class="col-sm-1 control-label">المادة</label>
                                <div class="col-sm-5">
                                    <select name="subject_id" id="subject_id" class="form-control ">
                                        @foreach($subjects as $sub)
                                            <option value="{{$sub->id}}" @if($sub->id == $group->subject_id) selected @endif >{{$sub->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <label for="edu_id" class="col-sm-1 control-label">المدرس</label>
                                <div class="col-sm-5">
                                    <select name="teacher_id" id="edu_id" class="form-control">
                                        @foreach($teachers as $teacher)
                                            <option value="{{$teacher->id}}" @if($teacher->id == $group->teacher_id) selected @endif>{{$teacher->fname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>




                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info center-block">تعديل المجموعة <i class="fa fa-save" style="margin-left: 5px"></i></button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->
                <!-- general form elements disabled -->

                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>

@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/jQuery-Tags-Input-master/dist/jquery.tagsinput.min.css')}}">
    <style>
        div.tagsinput span.tag {
            border: 1px solid #66c0e0;
            -moz-border-radius: 2px;
            -webkit-border-radius: 2px;
            display: block;
            float: left;
            padding: 5px;
            text-decoration: none;
            background: #66c0e0;
            color: #ffffff;
            margin-right: 5px;
            margin-bottom: 5px;
            font-family: helvetica;
            font-size: 14px;
            border-radius: 10px;
        }
        div.tagsinput span.tag a{
            color: white;
        }
    </style>
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css')}}">

    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice{
            background-color: #0d6aad;
            border: none;
        }
    </style>
@endsection

@section('js')
    <!-- CK Editor -->
    <script src="{{ asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>


    <script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor1')
            CKEDITOR.replace('editor2')
            CKEDITOR.replace('editor3')
            CKEDITOR.replace('editor4')
            CKEDITOR.replace('editor5')
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
    </script>
    <script src="{{ asset('assets/bower_components/jQuery-Tags-Input-master/dist/jquery.tagsinput.min.js')}}"></script>
    <script>
        $('#meta_keywords').tagsInput({
            // 'height':'34px',
            'width':'315px',
            'defaultText':'',
        });
    </script>


    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.min.js')}}"></script>

    <script>
        $('.select2').select2()
    </script>

@endsection
