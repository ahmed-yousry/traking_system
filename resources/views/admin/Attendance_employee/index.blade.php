@extends('admin.layouts.master')
@section('title')
حضور الموظفين
@endsection
@section('page-header')
    <section class="content-header">
        <h1>
            حضور وانصراف الموظفين
     <small></small></h1>

    </section>
@endsection

@section('content')


    <section class="content">

        <div class="row">
        <div class="col-md-12">

            {{--@if(\Illuminate\Support\Facades\Auth::guard('admin')->user()->can('classes.create'))--}}
                <a href="{{url('/admin/attendance-employee/create')}}" class="btn btn-primary pull-right margin-bottom">
                    <i class="fa fa-plus"></i>
                    تسجيل حضور
                </a>
            {{--@endif--}}

        </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">
                        </h3>
                        <div class="box-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control pull-right"
                                       placeholder="Search">

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default">
                                        <i class="fa fa-search"></i></button>
                                </div>
                            </div>

                                        <div class="input-group" id="adv-search">
                                            <input type="text" class="form-control" placeholder="Search for snippets" />
                                            <div class="input-group-btn">
                                                <div class="btn-group" role="group">
                                                    <div class="dropdown dropdown-lg">
                                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
                                                        <div class="dropdown-menu dropdown-menu-right" role="menu">
                                                            <form class="form-horizontal" role="form">
                                                                <div class="form-group">
                                                                    <label for="filter">Filter by</label>
                                                                    <select class="form-control">
                                                                        <option value="0" selected>All Snippets</option>
                                                                        <option value="1">Featured</option>
                                                                        <option value="2">Most popular</option>
                                                                        <option value="3">Top rated</option>
                                                                        <option value="4">Most commented</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="contain">Author</label>
                                                                    <input class="form-control" type="text" />
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="contain">Contains the words</label>
                                                                    <input class="form-control" type="text" />
                                                                </div>
                                                                <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                    <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                                </div>
                                            </div>
                                        </div>

                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th>اسم المدرس</th>
                                <th>الحالة</th>
                                <th>التاريخ</th>
                            </tr>
                            @foreach($employees as $employee)
                                <tr>
                                    <td>{{$employee->employee->fname}}</td>
                                    <td>
                                        @if($employee->state == "attend")
                                            حضور
                                        @else
                                            انصراف
                                        @endif
                                    </td>
                                    <td>
                                        {{$employee->created_at}}

                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">

                        <ul class="pagination pagination-sm no-margin pull-right">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">&raquo;</a></li>
                        </ul>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>


        <br>

    </section>

@endsection

@section('css')

@endsection

@section('js')


@endsection
