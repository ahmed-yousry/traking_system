@extends('admin.layouts.master')
@section('title')
    اضافة سنة دراسية جديدة
@endsection
@section('page-header')
    <section class="content-header">
        <h1>
            اضافة سنة دراسية جديدة
            <small></small>
        </h1>

    </section>
@endsection

@section('content')

    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="post" action="{{url('/admin/class/'.$class->id)}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input name="_method" type="hidden" value="PATCH">

                        <div class="box-body">

                            <div class="form-group">
                                <label for="title" class="col-sm-4 control-label">اسم السنة الدراسية</label>
                                <div class="col-sm-4 {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <input type="text" name="name" value="{{$class->name}}" class="form-control" id="title" placeholder="أسم السنة الدراسية" value="{{ old('name') }}" required autofocus>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>


                            </div>
                            <div class="form-group">
                                    <label for="education_year_id" class="col-sm-4 control-label">المرحلة الدراسية</label>

                                    <div class="col-sm-4">
                                        <select name="education_year_id" id="education_year_id" class="form-control select">
                                            @foreach($educations as $edu)
                                                <option value="{{$edu->id}}" @if($class->education_year_id == $edu->id) selected @endif>{{$edu->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                            </div>
                            <div class="form-group">
                                <label for="category" class="col-sm-4 control-label">المواد الخاصة بهذه السنة</label>
                                <div class="col-sm-4 ">
                                    <select name="subject_id[]" id="subject_id" class="select2 form-control " multiple>
                                        @foreach($subjects as $sub)
                                            <option value="{{$sub->id}}"
                                                    @foreach($class->subjects as $calss_sub)
                                                        @if($calss_sub->id == $sub->id)
                                                        selected
                                                        @endif
                                                    @endforeach
                                            >{{$sub->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info center-block">تعديل السنة <i class="fa fa-save" style="margin-left: 5px"></i></button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->
                <!-- general form elements disabled -->

                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>

@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/jQuery-Tags-Input-master/dist/jquery.tagsinput.min.css')}}">

    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css')}}">
    <style>
            .select2 {
                width:100%!important;
                }
                .select2-selection { overflow: hidden; }
.select2-selection__rendered { white-space: normal; word-break: break-all; }
    </style>
    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice{
            background-color: #0d6aad;
            border: none;
        }
    </style>
@endsection

@section('js')
    <!-- CK Editor -->
    <script src="{{ asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>


    <script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor1')
            CKEDITOR.replace('editor2')
            CKEDITOR.replace('editor3')
            CKEDITOR.replace('editor4')
            CKEDITOR.replace('editor5')
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
    </script>
    <script src="{{ asset('assets/bower_components/jQuery-Tags-Input-master/dist/jquery.tagsinput.min.js')}}"></script>
    <script>
        $('#meta_keywords').tagsInput({
            // 'height':'34px',
            'width':'315px',
            'defaultText':'',
        });
    </script>


    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.min.js')}}"></script>

    <script>
        $('.select2').select2()
    </script>

@endsection


