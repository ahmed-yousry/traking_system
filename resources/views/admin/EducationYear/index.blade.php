@extends('admin.layouts.master')
@section('title')
    المراحل الدراسية
@endsection
@section('page-header')
    <section class="content-header">
        <h1>
            المراحل الدراسية
     <small></small></h1>

    </section>
@endsection

@section('content')


    <section class="content">

        <div class="row">
        <div class="col-md-12">
            @if(\Illuminate\Support\Facades\Auth::guard('admin')->user()->can('education.create'))
                <a href="{{url('/admin/education/create')}}" class="btn btn-primary pull-right margin-bottom">
        <i class="fa fa-plus"></i>
        اضافة مرحلة جديدة
        </a>
            @endif

        </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    {{--<div class="box-header">--}}
                        {{--<h3 class="box-title">--}}
                        {{--</h3>--}}
                        {{--<div class="box-tools">--}}
                            {{--<div class="input-group input-group-sm" style="width: 150px;">--}}
                                {{--<input type="text" name="table_search" class="form-control pull-right"--}}
                                       {{--placeholder="Search">--}}

                                {{--<div class="input-group-btn">--}}
                                    {{--<button type="submit" class="btn btn-default">--}}
                                        {{--<i class="fa fa-search"></i></button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th>اسم المرحلة</th>
                                <th>عمليات</th>
                            </tr>
                            @foreach($educations as $edu)
                                <tr>
                                    <td>{{$edu->name}}</td>
                                    <td>
                                        @if(\Illuminate\Support\Facades\Auth::guard('admin')->user()->can('education.update'))
                                            <a href="{{url('/admin/education/'.$edu->id.'/edit')}}" class="btn btn-info btn-circle"><i class="fa fa-edit"></i></a>
                                        @endif
                                            @if(\Illuminate\Support\Facades\Auth::guard('admin')->user()->can('education.delete'))
                                                <a href="{{url('admin/education/'.$edu->id.'/delete')}}" class="btn btn-danger btn-circle"><i class="fa fa-trash-o"></i></a>
                                            @endif
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        {{ $educations->links() }}
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>

        <br>

    </section>

@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/lightbox2-master/lightbox.css')}}">
@endsection

@section('js')

    <script src="{{ asset('assets/bower_components/lightbox2-master/lightbox.js')}}"></script>

@endsection
