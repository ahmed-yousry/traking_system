@extends('admin.layouts.master')
@section('title')
  تعديل  رسائل@endsection

@section('page-header')
    <section class="content-header">
        <h1>
  تعديل رسائل            <small></small>
        </h1>

    </section>
@endsection

@section('content')

    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">   </h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    
                   
                    <form class="form-horizontal" method="post" action="{{url('/admin/message/show/edit').'/'.$group_message->id}}">
                        {{csrf_field()}}
                    <div class="box-body">
               
                         

               




     <div class="form-group">

<label for="title" class="col-sm-4 control-label">  العنوان </label>

    <div class="col-sm-4 {{ $errors->has('title') ? ' has-error' : '' }}">
        <input type="text" name="title" value="{{$group_message->title}}" class="form-control" id="title" placeholder="عنوان  الرساله" value="{{ old('title') }}">
        @if ($errors->has('title'))
            <span class="help-block">
<strong>{{ $errors->first('title') }}</strong>
</span>
        @endif
    </div>

</div>







     <div class="form-group">

<label for="content" class="col-sm-4 control-label">  المحتوى </label>

    <div class="col-sm-4 {{ $errors->has('content') ? ' has-error' : '' }}">
        <input type="text" name="content" value="{{$group_message->content}}" class="form-control" id="content" placeholder="محتوى الرساله" value="{{ old('content') }}">
        @if ($errors->has('content'))
            <span class="help-block">
<strong>{{ $errors->first('content') }}</strong>
</span>
        @endif
    </div>

</div>


                    
               
            
                         


              

                   
           
                    
                  
                  



                      



                 



                    </div>




                    <div class="box-footer">
                    <button type="submit" class="btn btn-info center-block">save</button>
                    </div>

                    </form>
                </div>

            </div>
        </div>
    </section>

@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css')}}">
@endsection

@section('js')

    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.min.js')}}"></script>


    <script>
        $('.select2').select2()
    </script>
@endsection


