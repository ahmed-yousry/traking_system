@extends('admin.layouts.master')
@section('title')
  اضافه  ولى لامر@endsection

@section('page-header')
    <section class="content-header">
        <h1>
اضافه ولى لامر           <small></small>
        </h1>

    </section>
@endsection

@section('content')

    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">   </h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->


                    <form class="form-horizontal" method="post" action="{{url('/admin/users/ConferenceTracks')}}">
                        {{csrf_field()}}
                    <div class="box-body">
                    <div class="form-group">

                    <label for="username" class="col-sm-4 control-label" > track name </label>

                        <div class="col-sm-4 {{ $errors->has('fname') ? ' has-error' : '' }}">
                            <input type="text" name="track_name" class="form-control" id="track_name" placeholder="track name" value="{{ old('track_name') }}">
                            @if ($errors->has('track_name'))
                                <span class="help-block">
                    <strong>{{ $errors->first('track_name') }}</strong>
                    </span>
                            @endif
                        </div>

                    </div>
















                    </div>




                    <div class="box-footer">
                    <button type="submit" class="btn btn-info center-block">save</button>
                    </div>

                    </form>
                </div>

            </div>
        </div>
    </section>

@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css')}}">
@endsection

@section('js')

    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.min.js')}}"></script>


    <script>
        $('.select2').select2()
    </script>
@endsection
