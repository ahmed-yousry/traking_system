@extends('admin.layouts.master')
@section('title')
    الامتحانات
@endsection
@section('page-header')
    <section class="content-header">
        <h1>
            السنين الدراسية
     <small></small></h1>
    </section>
@endsection

@section('content')

<!-- Top content -->
<div class="top-content">
        <div id="getting-started"></div>
    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 text">
                    <h1> امتحان الطالب </h1>                           
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 form-box">                        	
                    <form role="form" action="" method="post" class="form-inline registration-form">                        		
                        <fieldset>
                            <div class="form-top">
                                <!-- <div class="form-top-left">
                                    <h3>Step 1 / 3</h3>
                                    <p>Tell us who you are:</p>
                                </div>
                                <div class="form-top-right">
                                    <i class="fa fa-user"></i>
                                </div> -->
                            </div>
                            <div class="form-bottom">											
                                <div class="form-group">
                                    <label class="control-label col-sm-2">المادة</label>
                                    <div class="col-sm-10">
                                    <select class="form-control" name="subject">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                        <label class="control-label col-sm-2">المدرس</label>
                                        <div class="col-sm-10">
                                        <select class="form-control" name="teacher">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">المجموعة</label>
                                    <div class="col-sm-10">
                                        <select class="form-control"name="group">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">الامتحان</label>
                                    <div class="col-sm-10">
                                    <select class="form-control"name="exam">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="button" class="btn btn-next ">ابدأ</button>
                                </div>										
                            </div>
                        </fieldset>
                        
                        <fieldset>
                            <div class="form-top">
                                <div class="form-top-right">
                                    <h3>السؤال الاول</h3>
                                    <p>اختر الاجابة الصحيحة مما يل</p>
                                </div>	
                                <div class="form-top-left">
                                    <!-- <span>الوقت</span> -->
                                </div>	                        		
                            </div>
                            <div class="form-bottom">
                                <p> قامت حرب اكتوبر سنة .....</p>
                                <div class="radio">
                                    <label><input type="radio" name="optradio" checked>1973</label>
                                </div>
                                <div class="radio">
                                    <label><input type="radio" name="optradio">1983</label>
                                </div>
                                <div class="radio">
                                    <label><input type="radio" name="optradio">1978</label>
                                </div>		
                                <div class="text-center">	                       
                                    <button type="button" class="btn btn-next">Next</button>
                                </div>	
                            </div>
                        </fieldset>
                        
                        <fieldset>
                            <div class="form-top">
                                <div class="form-top-right">
                                    <h3>السؤال الاول</h3>
                                    <p>اختر الاجابة الصحيحة مما يل</p>
                                </div>	
                                <div class="form-top-left">
                                    <!-- <span>الوقت</span> -->
                                </div>	                        		
                            </div>
                            <div class="form-bottom">
                                <p> قامت حرب اكتوبر سنة .....</p>
                                <div class="radio">
                                    <label><input type="radio" name="optradio" checked>1973</label>
                                </div>
                                <div class="radio">
                                    <label><input type="radio" name="optradio">1983</label>
                                </div>
                                <div class="radio">
                                    <label><input type="radio" name="optradio">1978</label>
                                </div>		
                                <div class="text-center">	                       
                                    <button type="button" class="btn btn-next">Next</button>
                                </div>	
                            </div>
                        </fieldset>

                        <fieldset>
                                <div class="form-top">
                                    <div class="form-top-right">
                                        <h3>السؤال الاول</h3>
                                        <p>اختر الاجابة الصحيحة مما يلى</p>
                                    </div>	
                                    <div class="form-top-left">
                                        <!-- <span>الوقت:</span> -->
                                        <!-- <span class="timer"></span>												 -->
                                    </div>	                        		
                                </div>
                                <div class="form-bottom">
                                    <p> قامت حرب اكتوبر سنة .....</p>
                                    <div class="radio">
                                        <label><input type="radio" name="optradio" checked>1973</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optradio">1983</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optradio">1978</label>
                                    </div>		
                                    <div class="text-center">	                       
                                        <button type="button" class="btn">ارسال</button>
                                    </div>	
                                </div>
                            </fieldset>		                    
                        
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
    
</div>

@endsection

@section('css')
<link rel="stylesheet" href="{{asset('assets/exam/bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/exam/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/exam/css/form-elements.css')}}">
<link rel="stylesheet" href="{{asset('assets/exam/css/style.css')}}">

@endsection

@section('js')

<script src="{{asset('assets/exam/js/jquery-1.11.1.min.js')}}"></script>
<script src="{{asset('assets/exam/js/countdown.js')}}"></script>
<script src="{{asset('assets/exam/js/jquery.countdown360.min.js')}}"></script>
<script src="{{asset('assets/exam/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/exam/js/jquery.backstretch.min.js')}}"></script>
<script src="{{asset('assets/exam/js/retina-1.1.0.min.js')}}"></script>
<script src="{{asset('assets/exam/js/scripts.js')}}"></script>

@endsection
