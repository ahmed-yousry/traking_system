@extends('admin.layouts.master')
@section('title')
Manage Mails
  @endsection

@section('page-header')
    <section class="content-header">
        <h1>
        Manage Mails
           <small></small>
        </h1>

    </section>
@endsection

@section('content')

    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">   </h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->


                    <form class="form-horizontal" method="post" action="{{url('/admin/users/EmailSettings/show/edit').'/'.$id}}">
                        {{csrf_field()}}
                    <div class="box-body">



                    <!--  <div class="form-group">-->

                    <!--<label for="lname" class="col-sm-4 control-label">الاسم الثانى </label>-->

                    <!--    <div class="col-sm-4 {{ $errors->has('lname') ? ' has-error' : '' }}">-->
                    <!--        <input type="text" name="lname" class="form-control" id="lname" placeholder="لاسم الثانى" value="{{ old('lname') }}">-->
                    <!--        @if ($errors->has('lname'))-->
                    <!--            <span class="help-block">-->
                    <!--<strong>{{ $errors->first('lname') }}</strong>-->
                    <!--</span>-->
                    <!--        @endif-->
                    <!--    </div>-->

                    <!--</div>-->


                    <!--  <div class="form-group">-->

                    <!--<label for="tname" class="col-sm-4 control-label">الاسم الثالث </label>-->

                    <!--    <div class="col-sm-4 {{ $errors->has('tname') ? ' has-error' : '' }}">-->
                    <!--        <input type="text" name="tname" class="form-control" id="tname" placeholder="لاسم الثالث" value="{{ old('tname') }}">-->
                    <!--        @if ($errors->has('tname'))-->
                    <!--            <span class="help-block">-->
                    <!--<strong>{{ $errors->first('tname') }}</strong>-->
                    <!--</span>-->
                    <!--        @endif-->

                    <!--</div> </div>-->







                                                             <div class="form-group">

                                <label for="email" class="col-sm-4 control-label"> subject </label>

                                    <div class="col-sm-4 {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <input type="text" name="subject" class="form-control" id="subject" placeholder="subject" value="{{@$email_Templets->first()->subject}}"  >
                                        @if ($errors->has('subject'))
                                            <span class="help-block">
                                <strong>{{ $errors->first('subject') }}</strong>
                                </span>
                                        @endif
                                    </div>

                                </div>


                                <div class="form-group">

                                <label for="email" class="col-sm-4 control-label"> body </label>

                                <div class="col-sm-4 {{ $errors->has('body') ? ' has-error' : '' }}">
                                <input type="text" name="body" class="form-control" id="body" placeholder="body" value="{{@$email_Templets->first()->body}}"  >
                                @if ($errors->has('body'))
                                <span class="help-block">
                                <strong>{{ $errors->first('body') }}</strong>
                                </span>
                                @endif
                                </div>

                                </div>


















                    <div class="box-footer">
                    <button type="submit" class="btn btn-info center-block">save</button>
                    </div>

                    </form>
                </div>

            </div>
        </div>
    </section>

@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css')}}">
@endsection

@section('js')

    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.min.js')}}"></script>


    <script>
        $('.select2').select2()
    </script>
@endsection
