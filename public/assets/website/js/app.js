/*
    __Conect Me___________________________________
    |                                             |
    |--- UX / Ui Devlope By   : Ahmed Mohamed Gad |
    |--- Mobile : +201113478716                   |
    |--- Email : ahmedsoft220@gmail.com           |
    |_____________________________________________|
*/
// ----------Get Dom
var QSA = function (tag, name, value) {
    var Div = document.querySelectorAll(tag + ("[" + name + " = '" + value + "']"));
    return Div;
};
// ---------Create Dom
var CD = function (tag) {
    var DOM = document.createElement(tag);
    return DOM;
};
// ---------Theme Style
var CS = function (em, css) {
    if (css === void 0) { css = []; }
    em.setAttribute("style", css.join("; "));
};
// const fT = (tag, hol = 0) =>{
//     let GH = tag;
//     for(let DT = 0; DT < GH.length; DT++){
//         if(GH[DT] != undefined){
//             hol
//         }
//     }
// }
// ------------------Section Main For Login & SignUp
var bgPage = function () {
    var WD = window.innerHeight, BG = QSA("section", "bgpage", "bg-app"), BG2 = QSA("div", "bgpage", "reviews-a");
    for (var RG = 0; RG < BG.length; RG++) {
        CS(BG[RG], [
            "min-height:" + WD + "px",
        ]);
    }
    for (var CG = 0; CG < BG2.length; CG++) {
        CS(BG2[CG], [
            "min-height:" + WD + "px",
        ]);
    }
};
// -------------ParaGraph For Login & SignUp
var loginAuthor = function () {
    var GH = QSA("div", "author", "loginA")[0];
    if (GH != undefined) {
        var PH = QSA("p", "author", "p")[0];
        var DH = GH.querySelectorAll(".line-gr");
        for (var TG = 0; TG < DH.length; TG++) {
            CS(PH, [
                "min-height:" + DH[TG].clientHeight + "px",
                "line-height : 1.4em",
            ]);
        }
    }
};
//------------------Link Reviews Header
var aLink = function () {
    var RN = QSA("header", "sm-header", "header"), RV = QSA("a", "a-link", "reviewer"), VN = QSA("nav", "a-link", "nav");
    if (RN[0].clientWidth < 992) {
        console.log(VN[0]);
        var GF = document.createElement("a");
        GF.setAttribute("class", "rev-header btn btn-secondary mr-auto");
        for (var DT = 0; DT < RV.length; DT++) {
            if (RV[DT] != undefined) {
                GF.innerHTML = RV[DT].innerHTML;
                GF.setAttribute("href", RV[DT].getAttribute("href"));
                VN[0].insertBefore(GF, VN[0].firstElementChild);
                RV[DT].remove();
            }
        }
    }
};
// -----Menu icon
var menuBtn = document.querySelectorAll(".menu-btn");
var Menuicon = function () {
    var _loop_1 = function (D) {
        if (menuBtn[D] != undefined) {
            menuBtn[D].addEventListener("click", function () {
                menuBtn[D].classList.toggle("is--active");
                menuBtn[D].classList.add("is--clicked");
                setTimeout(function () {
                    menuBtn[D].classList.remove("is--clicked");
                }, 300);
            });
        }
    };
    for (var D = 0; D < menuBtn.length; D++) {
        _loop_1(D);
    }
};
// ---------Header Postion
var ScrollBody = function () {
    var f = document.querySelectorAll("html"), em = QSA("div", "sm-brand", "brand"), w = QSA("nav", "a-link", "nav");
    for (var u = 0; u < em.length; u++) {
        if (f[0].scrollTop > em[u].clientHeight) {
            w[0].setAttribute("n-style", "nav");
            em[u].setAttribute("style", "margin-bottom:" + w[0].clientHeight + "px");
        }
        else {
            w[0].removeAttribute("n-style");
            em[u].setAttribute("style", "margin-bottom:" + 0 + "px");
        }
    }
};
window.addEventListener("scroll", function () {
    ScrollBody();
});

var GT = function () {
    var t = document.getElementById("editor");
    if (t != undefined) {
        var quill = new Quill('#editor', {
            theme: 'snow',
            modules: {
                toolbar: [
                    [{ header: [1, 2, false] }],
                    ['bold', 'italic', 'underline'],
                    ['image', 'code-block']
                ]
            },
            placeholder: 'Compose an epic...'
        });
    }
};
var me = function () {
    var body = document.querySelectorAll("html");
    var head = document.querySelectorAll("head");
    var code = document.createComment(" |--- UX / Ui Develope By   : Ahmed Mohamed Gad");
    var mob = document.createComment(" |--- Mobile : +201113478716     ");
    var em = document.createComment(" |--- Email : ahmedsoft220@gmail.com   ");
    var code2 = document.createComment(" |--- Backend Develope By   : Ahmed Yousry");
    var em2 = document.createComment(" |--- Email : ayousry943@gmail.com   ");
    head[0].appendChild(code);
    head[0].appendChild(mob);
    head[0].appendChild(em);
    head[0].appendChild(code2);
    head[0].appendChild(em2);
};
var runAT = function () {
    var mAT = QSA("div", "coBtn", "coAuthor")[0];
    if (mAT != undefined) {
        mAT.addEventListener("click", function () {
            coAuthor_1();
        });
        var coAuthor_1 = function () {
            var mFM = QSA("form", "bt", "formMain")[0], divFm = QSA("div", "gName", "cGrand")[0], GT = CD("div");
            GT.setAttribute("class", "row w-100 sb-content m-0");
            GT.setAttribute("newDiv", "coAuthor");
            mFM.insertBefore(GT, mFM.childNodes[4]);
            var diF = QSA("div", "newdiv", "coAuthor");
            for (var TF = 0; TF < diF.length; TF++) {
                diF[TF].innerHTML = divFm.innerHTML;
            }
        };
    }
};
// -----------Run All Function
window.onload = function () {
    me();
    bgPage();
    loginAuthor();
    aLink();
    Menuicon();
    runAT();
    GT();
    ScrollBody();
    // QSA("div", "page", "loadingPage")[0].remove();
};
$(document).ready(function () {
    $(".search").keyup(function () {
        var searchTerm = $(".search").val();
        var listItem = $('.results tbody').children('tr');
        var searchSplit = searchTerm.replace(/ /g, "'):containsi('");
        $.extend($.expr[':'], { 'containsi': function (elem, i, match, array) {
                return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
            }
        });
        $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function (e) {
            $(this).attr('visible', 'false');
        });
        $(".results tbody tr:containsi('" + searchSplit + "')").each(function (e) {
            $(this).attr('visible', 'true');
        });
        var jobCount = $('.results tbody tr[visible="true"]').length;
        $('.counter').text(jobCount + ' item');
        if (jobCount == '0') {
            $('.no-result').show();
        }
        else {
            $('.no-result').hide();
        }
    });
});
