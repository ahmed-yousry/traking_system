<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendaceStudent extends Model
{
    //
    protected $table = 'attendace_students';
    protected $fillable = ['date','student_id','group_id'];

    public function student(){
        return $this->belongsTo('App\student','student_id');
    }
    public function group(){
        return $this->belongsTo('App\Group','group_id');
    }

    public function subject(){
        return $this->belongsTo('App\Subject','group_id');
    }

    public function teacher(){
        return $this->belongsTo('App\teacher','group_id');
    }


   


}
