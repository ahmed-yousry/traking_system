<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EducationYear extends Model
{
    //
    protected $table='education_years';
    protected $fillable = ['name'];

    public function classes(){
        return $this->hasMany('App\ClassRoom');
    }
}
