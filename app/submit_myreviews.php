<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\paper_reply;

class submit_myreviews extends Model
{
        protected $fillable = [
     
        'id',
        'Reviewer_expertise',
        'Originality',
        'Quality',
        'English_Quality',
        'Detailed_comments',
        'Comments_to_Proceedings',
        'Overall_evaluation',
        'user_id',
        'paper_id',        
        'paper_replies_id',
    


        ];
            protected $table = 'submit_myreviews';


            public function get_paper_reply(){

                return $this->belongsTo('App\paper_reply', 'paper_id', 'paper_id');
        
        }


          public function get_user(){

              

                return $this->belongsTo(User::class,'user_id');

        
        }

        

}


