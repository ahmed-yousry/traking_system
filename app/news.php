<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class news extends Model
{

protected $table = 'news';
protected $fillable = [
'name',
'number',
'user_id',


];
public function user_id(){
	return $this->hasOne('App\User','id','user_id');
	// return $this->hasOne('App\User', 'foreign_key', 'local_key');
	// return $this->belongsTo('App\User', 'foreign_key', 'other_key');
}
public function comments(){
	return $this->hasMany('App\comments','news_id','id');
// has many one to many  relation it will call comments table to  select news_id from id  
}

public function count_comments(){
	return $this->hasMany('App\comments','news_id','id');
}



}
