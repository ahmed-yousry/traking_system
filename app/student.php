<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class student extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */

    use SoftDeletes;

    protected $fillable = [
     
'fname',
'phone',
'deleted_at',
'created_at',
'email',
'group_id'


];
    protected $dates = ['deleted_at'];


    protected $table = 'students';
    

    public function classes(){
	    return $this->belongsTo(ClassRoom::class, 'class_room_id');
}

public function student(){
    return $this->belongsTo(student::class, 'id');
}

public function group(){
    return $this->belongsTo(student_group::class, 'group_id');
}
    public function mom(){
        return $this->belongsTo(mom::class, 'mom_id');
    }



    public function attendances(){
        return $this->hasMany('App\AttendanceStudent');

    }

    public function student_group(){
        return $this->belongsToMany('App\Group','student_groups','student_id','group_id')->withTimestamps();
    }

}

