<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
     
     protected $fillable = [
     
'user_id',
'content',
'seg',
'created_at ',
'updated_at',
'title',
];

    protected $table = 'Notifications';
    



}

