<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceTeacher extends Model
{
    //
    protected $fillable=['teacher_id','state'];

    public function teacher(){
        return $this->belongsTo('App\teacher','teacher_id');
    }

}
