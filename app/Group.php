<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    //
    protected $table = 'groups';
    protected $fillable = ['name','subject_id','from','to','class_room_id','teacher_id'];

    public function classes(){
        return $this->belongsTo('App\ClassRoom','class_room_id');
    }
    public function subject(){
        return $this->belongsTo('App\Subject','subject_id');
    }
     public function teacher(){
         return $this->belongsTo('App\teacher','teacher_id');
     }
     public function attendances(){
         return $this->hasMany('App\AttendanceStudent');
     }

    public function student_group(){
        return $this->belongsToMany('App\student','student_groups','group_id','student_id')->withTimestamps();
    }

}
