<?php

namespace App\Http\Controllers\userController;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Assign_reviewer;
use App\submit_papers;

class reviewer_accept extends Controller
{
    public function index(Request $request,$id)
    {
       $paper = submit_papers::where('id','=',$request->id)->with('get_metadata')->get()->last();
  
        // $permissions = Permission::all();
        return view('user.reviews.reviewer_accept', compact('id','paper'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->accept == 'accept') {

            $daynow = \Carbon\Carbon::now();
            $today = \Carbon\Carbon::today();

            $Due = $daynow->addDays(7);
            // dd($daynow);

            Assign_reviewer::where('paper_id', $request->id)->update([
                'accept_date' => $today,
                'Due' => $Due
            ]);
        }

        return redirect('/home');

        // $reviewer_accept->save();
    }
}
