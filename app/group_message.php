<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class group_message extends Model
{
    protected $fillable = [
     
        'teacher_id',
        'group_id',
        'title',
        'created_at',
        'content',
        'seen'
        
        
        ];
        
        
        protected $table = 'group_messages';
            
        
            public function teacher_id(){
                return $this->belongsTo(teacher::class, 'teacher_id');
        }

        public function group_id(){
            return $this->belongsTo(Group::class, 'group_id');
    }


        
}
