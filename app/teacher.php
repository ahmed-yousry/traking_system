<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class teacher extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
     
     protected $fillable = [
     
'fname',
'phone',
'seg',
'deleted_at',
'created_at'


];

    protected $table = 'teachers';

    public function groups(){
        return $this->hasMany('App\ClassRoom');
    }
    public function subjects(){
        return $this->belongsToMany('App\Subject','teacher_subject','teacher_id','subject_id')->withTimestamps();
    }
    public function attendances(){
        return $this->hasMany('App\AttendanceTeacher');

    }
}
