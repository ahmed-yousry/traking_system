<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Psr\Container\ContainerInterface;
//use Auth;
use App\Notification;

Auth::routes();


//for user routes
// Route::get('/', function () {
//     return view('auth.login');
// });
//

      


Route::get('/', 'ShortLinkController@index');

Route::post('/', 'ShortLinkController@store')->name('generate.shorten.link.post');



Route::get('{code}', 'ShortLinkController@shortenLink')->name('shorten.link');

Route::get('ShowReport/{id}', 'ShortLinkController@ShowReport')->name('report.link');














Route::get('/send-email', function() {
    $output = [];
    \Artisan::call('queue:work', $output);
    dd($output);
});




  Route::get('/cache-clear', function() {
        $status = Artisan::call('cache:clear');
        return '<h1>Cache cleared</h1>';
      });



 Route::get('test-send-email', function () {




  $emaildata  = array('blade-path' => 'email.confirmation',
           'to'=>'ayousry943@gmail.com',
           'from'=>'info@ierek.net',
           'cc'=>'test@ierek.net',
           'subject'=>'test from  job  ',
            );



    $job = (new \App\Jobs\sendmailjob($emaildata))->delay( \Carbon\Carbon::now()->addSeconds(2) );

    dispatch($job);

    return  'test  send  email  ';
});













// //SocialLite
// Route::get('login/{provider}', 'AuthSocController@redirectToProvider');
// Route::get('login/{provider}/callback', 'AuthSocController@handleProviderCallback');











///////////////////////////////////////////











/// user route

/// USER Routes


Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');

Route::get('/user/resend/verify/{id}', 'Auth\RegisterController@reverifyUser');



Route::get('/home', 'HomeController@index')->name('home');

Route::get('home/user/logout', function () {
    Auth()->guard('web')->logout();
    return redirect('/login');
});



Route::get('reviewer/accept/{id}','userController\reviewer_accept@index');
Route::post('reviewer/accept','userController\reviewer_accept@create');

Route::group(['prefix' => LaravelLocalization::setLocale().'/home','middleware'=>'auth:web'],function(){


   Route::get('submit/newpaper','userController\SubmitNewPaper@index');

    Route::get('Profile','userController\Profile@index');
    Route::post('Profile/{id}','userController\Profile@update');


 Route::get('submit','userController\submit_paper@index');
 Route::post('submit','userController\submit_paper@create');

  Route::get('metadata/{id}','userController\metadatas@index');
 Route::post('metadata/{id}','userController\metadatas@create');

 Route::get('edit/metadata/{id}','userController\metadatas@edit');
 Route::post('metadata/edit/{id}','userController\metadatas@editform');

 Route::get('confirmation','userController\confirmation@index');

Route::get('auther_paper','userController\auther_paper@index');

  Route::get('reviews','userController\reviews@index');


    Route::get('submit_myreview/{id}','userController\submit_myreview@index');
  Route::post('submit_myreview/{id}','userController\submit_myreview@create');




   Route::get('submit_myreview/edit/{id}','userController\submit_myreview@edit');
 Route::post('submit_myreview/edit/{id}','userController\submit_myreview@update');



    Route::get('reviews-show-data/{id}','userController\submit_myreview@show');



    Route::get('auther-show-data/{id}','userController\submit_auther_review@index');

     Route::post('auther-submit-replay/{id}','userController\submit_auther_review@create');




//editor


  Route::get('editor','userController\editor@index');

  Route::get('editor/in-review','userController\editor@inreview');

  Route::get('editor/View_Submission/{id}','userController\editor@View_Submission');

  Route::get('editor/Archive','userController\editor@Archive');



Route::get('editor/main-review/{id}','userController\mainreview@index');



Route::get('editor/selectreview/{id}','userController\selectreview@index');

Route::post('editor/selectreview/{id}','userController\selectreview@create');


Route::get('editor/sendemail/{id}/{user_id}','userController\sendemail@index');

Route::post('editor/sendemail','userController\sendemail@create');


Route::get('editor/Review/Reviewer/{user_id}','userController\sendemail@index');

Route::get('editor/Review/Reviewer/Acknowledge/{user_id}','userController\Acknowledge_sendemail@index');

Route::post('editor/Review/Reviewer/Acknowledge/','userController\Acknowledge_sendemail@create');

Route::post('editor/Review/Editor_Decision/{user_id}','userController\Editor_Decision@create');

Route::get('editor/Review/See_Reviewer_Review/{paper_id}/{user_id}','userController\decisioncomments@index');

Route::get('editor/Review/sendemail/toauther/{paper_id}/{user_id}','userController\SendEmailToAuther@index');

Route::post('editor/Review/sendemail/toauther/','userController\SendEmailToAuther@create');



Route::get('editor/Review/editer_auther_comment/{paper_id}/{user_id}','userController\editer_auther_comment@index');

Route::post('editor/Review/editer_auther_comments','userController\editer_auther_comment@create');





Route::get('editor/Review/auther_editer_comment/{paper_id}','userController\auther_editer_comment@index');

Route::post('editor/Review/auther_editer_comment','userController\auther_editer_comment@create');


 });










//Admin Routes
Route::get('/admin/login', 'admin_auth@index');
Route::post('/admin/login', 'admin_auth@loginpost')->name('admin.login.submit');

Route::get('/admin/forget/password','admin_auth@forgetpassword');
Route::get('/admin/reset/password/{token}','admin_auth@resetpassword');
Route::post('/admin/reset/password/{token}','admin_auth@postresetpassword');
Route::post('/admin/forget/password','admin_auth@postforgetpassword');


Config::set('auth.defines','admin');
Route::get('admin/logout', function () {
    Auth()->guard('admin')->logout();
    return redirect('/admin/login');
});





///admin route
 Route::group(['prefix' => LaravelLocalization::setLocale().'/admin','middleware'=>'admin:admin'],function(){


     Route::get('','AdminController\AdminController@index')->name('admin.dashboard');


     //Slider Route
     Route::resource('slider','AdminController\SliderController');
     Route::get('slider/{id}/delete','AdminController\SliderController@destroy');

     //Setting Route
     Route::get('setting','AdminController\settingController@index');
     Route::post('setting','AdminController\settingController@store');

     //Education Route
     Route::resource('education','AdminController\EducationController');
     Route::get('education/{id}/delete','AdminController\EducationController@destroy');

     //Class Route
     Route::resource('class','AdminController\ClassController');
     Route::get('class/{id}/delete','AdminController\ClassController@destroy');

     //Subject Route
     Route::resource('subject','AdminController\SubjectController');
     Route::get('subject/{id}/delete','AdminController\SubjectController@destroy');

     //Group Route
     Route::resource('group','AdminController\GroupController');
     Route::get('group/{id}/delete','AdminController\GroupController@destroy');

     //     attendance-student
     Route::resource('attendance-student','AdminController\AttendaceStudentController');
     Route::get('attendance-student/{id}/delete','AdminController\AttendaceStudentController@destroy');

     //     attendance-teacher
     Route::resource('attendance-teacher','AdminController\AttendanceTeacherController');
     Route::get('attendance-teacher/{id}/delete','AdminController\AttendanceTeacherController@destroy');

     //     attendance-employee
     Route::resource('attendance-employee','AdminController\AttendanceEmployeeController');
     Route::get('attendance-employee/{id}/delete','AdminController\AttendanceEmployeeController@destroy');




     //Users
     Route::get('users','AdminController\AdminController@getAllUsers');
     Route::get('users/{id}/delete','AdminController\AdminController@deleteUser');


//users-auther-syatem
     Route::get('users/show/{search?}','AdminController\createusers@showusers');
      Route::get('users/show/delete/{id}','AdminController\createusers@deleteusers');
       Route::get('users/show/edit/{id}','AdminController\createusers@editusers');
       Route::post('users/show/edit/{id}','AdminController\createusers@posteditusers');


     Route::get('users/add','AdminController\createusers@addusers');
     Route::post('users/add','AdminController\createusers@addformusers');
//users-auther-syatem


     //PaperTemplate

       Route::get('users/PaperTemplate','AdminController\paper_example@editPaperTemplate');

       Route::post('PaperTemplate/{id}','AdminController\paper_example@posteditPaperTemplate');


     Route::get('mom/add','AdminController\createmom@addmom');
     Route::post('mom/add','AdminController\createmom@addformmom');





//ConferenceTracks

     Route::get('users/ConferenceTracks','AdminController\ConferenceTracks@addConferenceTracks');
     Route::post('users/ConferenceTracks','AdminController\ConferenceTracks@addformConferenceTracks');


     Route::get('users/ConferenceTracks/show','AdminController\ConferenceTracks@showConferenceTracks');


     Route::get('users/ConferenceTracks/show/edit/{id}','AdminController\ConferenceTracks@editConferenceTracks');
     Route::post('users/ConferenceTracks/show/edit/{id}','AdminController\ConferenceTracks@addformConferenceTracks');



     Route::get('users/ConferenceTracks/show/delete/{id}','AdminController\ConferenceTracks@deleteTrack_Selction');


/////


//EmailSettings




Route::get('users/EmailSettings','AdminController\EmailSettings@index');
Route::post('users/EmailSettings','AdminController\EmailSettings@addforemployee');
Route::get('users/EmailSettings/show/edit/{id}','AdminController\EmailSettings@editEmailSettings');
Route::post('users/EmailSettings/show/edit/{id}','AdminController\EmailSettings@posteditEmailSettings');





//ConferenceImportantDates
     Route::get('users/ConferenceImportantDates/show/{search?}','AdminController\ConferenceImportantDate@showemployee');
     Route::get('users/ConferenceImportantDates/show/delete/{id}','AdminController\ConferenceImportantDate@deleteemployee');


     Route::get('users/ConferenceImportantDates','AdminController\ConferenceImportantDate@index');
     Route::post('users/ConferenceImportantDates','AdminController\ConferenceImportantDate@addforemployee');
     Route::get('users/ConferenceImportantDates/show/edit/{id}','AdminController\ConferenceImportantDate@editConferenceImportantDates');
     Route::post('users/ConferenceImportantDates/show/edit/{id}','AdminController\ConferenceImportantDate@posteditConferenceImportantDates');



//invoice
Route::get('invoice/show/{search?}','AdminController\createinvoice@showinvoice');

   // Route::get('invoice/show/delete/{id}','AdminController\createemp@deleteemployee');


   Route::get('invoice/{id}','AdminController\createinvoice@addinvoice');

   Route::post('invoice/{id}','AdminController\createinvoice@addforinvoice');

     Route::get('invoice/show/edit/{id}','AdminController\createinvoice@editinvoice');

      Route::post('invoice/show/edit/{id}','AdminController\createinvoice@posteditinvoice');




       Route::get('exam',function(){
        return view('admin.exam.exam');
    });




//message

Route::get('message','AdminController\createmessage@addmessage');
Route::post('message','AdminController\createmessage@postaddmessage');
Route::get('show/message','AdminController\createmessage@showmessage');

Route::get('message/show/edit/{id}','AdminController\createmessage@editmessage');

Route::post('message/show/edit/{id}','AdminController\createmessage@posteditmessage');
Route::get('message/show/delete/{id}','AdminController\createmessage@deletemessage');




//invoice

     Route::get('exam/show/{search?}','AdminController\createinvoice@showinvoice');
     //Route::get('invoice/show/delete/{id}','AdminController\createinvoice@deleteemployee');

     Route::get('exam/{id}','AdminController\createexam@addexam');
     Route::post('exam/{id}','AdminController\createexam@addforexam');
    //   Route::get('exam/show/edit/{id}','AdminController\createinvoice@editexam');
    //    Route::post('exam/show/edit/{id}','AdminController\createinvoice@posteditexam');



// for user managers routes
     Route::resource('user/manager','AdminController\UserManagerController');
     Route::get('user/manager/{id}/delete','AdminController\UserManagerController@destroy');



     // for managers routes
          Route::resource('manager','AdminController\managerController');
          Route::get('manager/{id}/delete','AdminController\managerController@destroy');




// for role  routes
     Route::resource('role','AdminController\roleController');
     Route::get('role/{id}/delete','AdminController\roleController@destroy');


     // for user  role routes
          Route::resource('user/role','AdminController\UserRoleController');
          Route::get('user/role/{id}/delete','AdminController\UserRoleController@destroy');



// for permission routes
     Route::resource('permission','AdminController\permissionController');
     Route::get('permission/{id}/delete','AdminController\permissionController@destroy');




 });






/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//


Route::get('/ahmed',function (){



    dd(\Mcamara\LaravelLocalization\Facades\LaravelLocalization::getSupportedLocales());

});



Route::get('/order', function () {
    return view('front.order');
});

Route::get('/welcome/{locale}', function ($local) {
    \Illuminate\Support\Facades\App::setLocale($local);
    return view('welcome');
});





























//////////////////////////////////////////
/*
 *
 *

Route::group(['middleware'=>'news'],function(){


Route::get('insert', 'newscontroller@showdb');
Route::post('insertdb', 'newscontroller@insertdb');
});



Route::get('send/mail', function () {
//Mail::to('ayousry943@gmail.com')->send(new App\Mail\testmail());
//  \App\Jobs\sendmailjob::dispatch();

  $job = (new \App\Jobs\sendmailjob)->delay(\Carbon\Carbon::now()->addSeconds(1));
  dispatch($job);
  return  'mail sent';
});

 */

//Route::get('data/user', function () {
//
//if (Gate::allows('showdata',auth()->user())) {
// return  view('welcome');
//}else{
//  return  'you dont have pertmation  ';
//}
//});
